from fastapi import FastAPI, Request, Depends
from fastapi.staticfiles import StaticFiles
from fastapi.responses import RedirectResponse, PlainTextResponse
from starlette.exceptions import HTTPException as StarletteException
from templates import template
from routes import authRoutes, blacklistRoutes, inpatientRoutes, mailerRoutes, stationRoutes, healthFormRoutes, receptionistRoutes, passRoutes, appointmentRoutes, smsRoutes, userRoutes, visitRoutes, visitorRoutes

app = FastAPI()
# Mount static folder
app.mount('/static', StaticFiles(directory='static'), name='static')
app.mount('/storage', StaticFiles(directory='storage'), name='storage')

# Register Routes
app.include_router(authRoutes.router)
app.include_router(receptionistRoutes.router)
app.include_router(visitorRoutes.router)
app.include_router(appointmentRoutes.router)
app.include_router(passRoutes.router)
app.include_router(stationRoutes.router)
app.include_router(visitRoutes.router)
app.include_router(userRoutes.router)
app.include_router(healthFormRoutes.router)
app.include_router(blacklistRoutes.router)
app.include_router(mailerRoutes.router)
app.include_router(smsRoutes.router)
app.include_router(inpatientRoutes.router)


# TODO: CREATE HANDLERS
@app.exception_handler(StarletteException)
def http_exception_handler(request, exc):
    if exc.status_code == 401:
        return RedirectResponse('/')
    else:
        return PlainTextResponse(content=str(exc.detail),status_code=exc.status_code)


@app.get('/visiting_schedule')
def visiting_schedule(request: Request):
    return template.TemplateResponse('/contents/public/visiting_schedule.html',{
    'request': request,
    'page': 'visiting_schedule',
    'title': 'Visiting Schedule'
})

@app.get('/visiting_procedures')
def visiting_procedures(request: Request):
    return template.TemplateResponse('/contents/public/visiting_procedures.html',{
    'request': request,
    'page': 'visiting_procedures',
    'title': 'Visiting Procedures'
})

@app.get('/visiting_policies_requirements')
def visiting_policies_requirements(request: Request):
    return template.TemplateResponse('/contents/public/visiting_policies_requirements.html',{
    'request': request,
    'page': 'visiting_policies_requirements',
    'title': 'Visiting Policies and Requirements'
})

@app.get('/')
def index(request: Request):
    return template.TemplateResponse('index.html', {
        'request': request,
        'title': 'Home'
    })


