from typing import Optional
from pydantic import BaseModel
from datetime import date, datetime

class AuthForm(BaseModel):
    email: str
    password: str
    remember: bool

class RegisterForm(BaseModel):
    email: str
    password: str
    user_type: str = 'Visitor'

class ProfileForm(BaseModel):
    first_name: str
    middle_name: Optional[str] = None
    last_name: str
    suffix_name: Optional[str] = None
    full_name: str
    
    birth_date: date
    gender: str
    contact_number: str
    
    house_street: str
    barangay: str
    municipality: str
    province: str
    region: str
    full_address: str

class RejectAppointment(BaseModel):
    remarks: str

class VisitUpdate(BaseModel):
    remarks: str

class ValidatePass(BaseModel):
    pass_id: str

class WalkinAppointment(BaseModel):
    full_name: str
    email: str
    contact_number: str
    birth_date: date
    address: str
    patient_name: Optional[str] = None
    station_id: str
    purpose: str
    schedule: date = date.today()
    status: str = 'Approved'
    date_submitted: datetime = datetime.today()
    type: str = 'Walk-In'

    symptoms: Optional[str] = None
    condition: str

class CheckinForm(BaseModel):
    pass_id: str
    remarks: str
    image: str

class CheckoutForm(BaseModel):
    pass_id: str

class OnlineAppointment(BaseModel):
    station_id: str
    patient_name: Optional[str] = None
    schedule: date
    purpose: str
    date_submitted: date = date.today()
    type: str = 'Online'

class HealthForm(BaseModel):
    appointment_id: str
    symptoms: Optional[str] = None
    condition: str
    date_submitted: date = date.today()

class AddBlacklist(BaseModel):
    remarks: str
    image: str
    visit: str

class UpdateBlacklistRemarks(BaseModel):
    remarks: str

class MailForm(BaseModel):
    date: date
    name: str
    message: str

class SMSForm(BaseModel):
    hazard: str
    station: str


