from fastapi import Cookie, HTTPException
from jose import jwt, JWTError
import re


secret = 'a very shady secret'

def get_token(token: str = Cookie('token')):
    try:
        user = jwt.decode(token, secret)
        if user:
            return user
    except JWTError:
        raise HTTPException(401, 'Please Log In first')

def is_guest(token: str = Cookie('token')):
    x = re.match('(^[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*$)', token)
    if x:
        user = jwt.decode(token, secret)
        return user.get('user_type')
    else:
        return True