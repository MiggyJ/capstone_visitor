# CAPSTONE PROJECT - Visitor Management System

Members (BSIT 3-1 A.Y. 2020-2021 SUMMER, Group 7)
 - Balbuena
 - Buena
 - Laberinto
 - Montante
 - Ordanza
 - Ricamara

# To Get Files
- Clone (if you don't have a copy yet)
    - Create an empty folder and open with terminal (cmd or powershell)
    - Run `git clone https://gitlab.com/MiggyJ/capstone_visitor.git .`
- Pull (if you already have a copy)
    - `git pull origin main`

# To Run
1. Create virtual environment
    - `python -m venv venv`
    - `venv\scripts\activate`
2. Install dependencies
    - `pip install -r requirements.txt`
3. Run migrations and seeders for 
    - Before you run this command, make sure have a database named "visitor_management".
    - `alembic upgrade head`
    - Note: if you need to reset data, run `alembic downgrade base` then migrate again.
4. Run server
    - `uvicorn main:app --reload`