from sqlalchemy import Text, String, Boolean, Date, DateTime, text
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from database import Base

class User(Base):
    __tablename__ = 'users'

    id = Column(String(36), primary_key=True, nullable=False, default=text('UUID()'))
    email = Column(String(255), nullable=False)
    password = Column(String(255), nullable=False)
    user_type = Column(String(255), nullable=False)
    station_id = Column(String(36), ForeignKey('stations.id'))
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))
    is_blacklist = Column(Boolean, default=text('0'))

    @hybrid_property
    def roles(self):
        return self.user_type.split(';')

    profile = relationship('User_Profile', back_populates='user', uselist=False)
    health_form = relationship('Health_Form', back_populates='user')
    appointment = relationship('Appointment', back_populates='user')
    user_pass = relationship('Pass', back_populates='user')
    station = relationship('Station')
    blacklist = relationship('Blacklist', back_populates='user', order_by='desc(Blacklist.created_at)')
    visits = relationship('Visit', secondary='join(Pass, Visit, Pass.id == Visit.pass_id)', secondaryjoin='Visit.pass_id == Pass.id', viewonly=True)

class User_Profile(Base):
    __tablename__ = 'user_profiles'

    id = Column(String(36), primary_key=True, nullable=False, default=text('UUID()'))
    user_id = Column(String(36), ForeignKey('users.id'), nullable=True)
    first_name = Column(String(255), nullable=False)
    middle_name = Column(String(255), nullable=True)
    last_name = Column(String(255), nullable=False)
    suffix_name = Column(String(255), nullable=True)
    birth_date = Column(Date, nullable=False)
    gender = Column(String(255), nullable=False)
    house_street = Column(String(255), nullable=False)
    barangay = Column(String(255), nullable=False)
    municipality = Column(String(255), nullable=False)
    province = Column(String(255), nullable=False)
    region = Column(String(255), nullable=False)
    contact_number = Column(String(255), nullable=False)
    full_name = Column(String(255), nullable=False)
    full_address = Column(String(255), nullable=False)

    user = relationship('User', back_populates='profile')

class Station(Base):
    __tablename__ = 'stations'

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    name = Column(String(255), nullable=False)
    location = Column(String(255), nullable=False)

    appointment = relationship('Appointment', back_populates='station')

class Health_Form(Base):
    __tablename__ = 'health_forms'

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    user_id = Column(String(36), ForeignKey('users.id'), nullable=True)
    full_name = Column(String(255), nullable=True)
    email = Column(String(255), nullable=True)
    symptoms = Column(String(255), nullable=True)
    condition = Column(String(255), nullable=False)
    date_submitted = Column(Date, nullable=False, default=text('NOW()'))

    user = relationship('User', back_populates='health_form')
    health_pass = relationship('Pass', back_populates='health_form')

class Appointment(Base):
    __tablename__ = 'appointments'

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    user_id = Column(String(36), ForeignKey('users.id'), nullable=True)
    station_id = Column(String(36), ForeignKey('stations.id'), nullable=True)
    full_name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    contact_number = Column(String(255), nullable=False)
    birth_date = Column(Date, nullable=False)
    address = Column(String(255), nullable=False)
    patient_name = Column(String(255), nullable=True)
    purpose = Column(String(255), nullable=False)
    schedule = Column(Date, nullable=False)
    status = Column(String(255), default='Pending')
    date_submitted = Column(Date, nullable=False)
    type = Column(String(255), nullable=False)
    file = Column(String(255), nullable=True)
    remarks = Column(Text, nullable=True)
    is_allowed = Column(Boolean, nullable=True)

    user = relationship('User', back_populates='appointment')
    station = relationship('Station', back_populates='appointment')
    appointment_pass = relationship('Pass', back_populates='appointment')
    health_form = relationship('Health_Form', secondary='join(Health_Form, Pass, Pass.health_form_id == Health_Form.id)', secondaryjoin='Health_Form.id == Pass.health_form_id', uselist=False, viewonly=True)
    visit = relationship('Visit', secondary='join(Pass, Visit, Visit.pass_id == Pass.id)', secondaryjoin='Pass.id == Visit.pass_id', uselist=False, viewonly=True)

class Pass(Base):
    __tablename__ = 'passes'

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    user_id = Column(String(36), ForeignKey('users.id'), nullable=True)
    appointment_id = Column(String(36), ForeignKey('appointments.id'), nullable=True)
    health_form_id = Column(String(36), ForeignKey('health_forms.id'), nullable=True)
    full_name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)

    user = relationship('User', back_populates='user_pass')
    appointment = relationship('Appointment', back_populates='appointment_pass')
    health_form = relationship('Health_Form', back_populates='health_pass')
    visit = relationship('Visit', back_populates='visit_pass')

class Visit(Base):
    __tablename__ = 'visits'

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    pass_id = Column(String(36), ForeignKey('passes.id'), nullable=True)
    image = Column(String(255), nullable=False)
    remarks = Column(Text, nullable=False)
    check_in = Column(DateTime, default=text('NOW()'))
    check_out = Column(DateTime, nullable=True)

    visit_pass = relationship('Pass', back_populates='visit')
    appointment = relationship('Appointment', secondary='join(Pass, Appointment, Pass.appointment_id == Appointment.id)', secondaryjoin='Appointment.id == Pass.appointment_id', uselist=False, viewonly=True)
    health_form = relationship('Health_Form', secondary='join(Pass, Health_Form, Pass.health_form_id == Health_Form.id)', secondaryjoin='Health_Form.id == Pass.health_form_id', uselist=False, viewonly=True)
    user = relationship('User', secondary='join(Pass, User, Pass.user_id == User.id)', secondaryjoin='User.id == Pass.user_id', uselist=False, viewonly=True)

class Blacklist(Base):
    __tablename__ = 'blacklists'

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    user_id = Column(String(36), ForeignKey('users.id'), nullable=True)
    full_name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    birth_date = Column(Date, nullable=False)
    image = Column(String(255), nullable=True)
    remarks = Column(Text, nullable=False)
    is_active = Column(Boolean, default=text('1'))
    is_seen = Column(Boolean, default=text('0'))
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    user = relationship('User', back_populates='blacklist', uselist=False)
    profile = relationship('User_Profile', secondary='join(User, User_Profile, User.id == User_Profile.user_id)', secondaryjoin='User_Profile.user_id == User.id', viewonly=True, uselist=False)

class Inpatient(Base):
    __tablename__ = 'inpatients'

    id = Column(String(36), primary_key=True, nullable=False, default=text('UUID()'))
    full_name = Column(String(255), nullable=False)
    first_name = Column(String(255), nullable=False)
    middle_name = Column(String(255), nullable=True)
    last_name = Column(String(255), nullable=False)
    is_accepting_visits = Column(Boolean, default=text('1'))
