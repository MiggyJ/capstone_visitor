from fastapi import Request, APIRouter, Depends, HTTPException, Form
from sqlalchemy import or_
from sqlalchemy.sql import func
from sqlalchemy.orm import Session, joinedload
from datatables import DataTable
import os
import base64 as b64
import datetime as dt
from sqlalchemy.sql.expression import asc, desc
from database import get_db, visit_count_per_day, visit_count_per_month, visits_per_department
from models import Station, Visit, Pass, Appointment
from schemas import CheckoutForm, VisitUpdate, CheckinForm
from templates import template
from dependencies import get_token
from functions import generate_pass

router = APIRouter(
    prefix='/visits',
    tags=['visits'],
    dependencies=[Depends(get_token)]
)

#? VISITS

#* GET
#* @Datatables Visits All
@router.get('/datatable/all')
def allDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Appointment.full_name.like(f'%{user_input}%'),
                    Appointment.contact_number.like(f'%{user_input}%'),
                    func.date_format(Visit.check_in, '%M %d, %Y %h:%m %p').like(f'%{user_input}%'),
                    func.date_format(Visit.check_out, '%M %d, %Y %h:%m %p').like(f'%{user_input}%'),
                    Appointment.schedule.like(f'%{user_input}%'),
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Visit, 
            db.query(Visit).\
                filter(
                    Appointment.type == 'Online',
                    Station.name == token.get('station')
                ), [
            ('image', 'image'),
            ('full_name', 'appointment.full_name'),
            ('contact_number', 'appointment.contact_number'),
            'check_in',
            'check_out',
            'id',
            ('is_blacklist', 'user.is_blacklist'),
            ('user_id', 'user.id')
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))


        return table.json()
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
#* @Datatables Walk-In
@router.get('/datatable/walkin')
def walkinDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Appointment.full_name.like(f'%{user_input}%'),
                    Appointment.contact_number.like(f'%{user_input}%'),
                    func.date_format(Visit.check_in, '%M %d, %Y %h:%m %p').like(f'%{user_input}%'),
                    func.date_format(Visit.check_out, '%M %d, %Y %h:%m %p').like(f'%{user_input}%'),
                    Appointment.schedule.like(f'%{user_input}%'),
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Visit, 
            db.query(Visit).\
                filter(
                    Appointment.type == 'Walk-In',
                    Station.name == token.get('station')
                ), [
            'image',
            ('full_name', 'appointment.full_name'),
            ('contact_number', 'appointment.contact_number'),
            'check_in',
            'check_out',
            'id'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))


        return table.json()
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
#* @Datatables Visits Today 
@router.get('/datatable/today')
def todayDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Appointment.full_name.like(f'%{user_input}%'),
                    func.date_format(Visit.check_in, '%M %d, %Y %h:%m %p').like(f'%{user_input}%'),
                    func.date_format(Visit.check_out, '%M %d, %Y %h:%m %p').like(f'%{user_input}%'),
                    Appointment.schedule.like(f'%{user_input}%'),
                )
            )

        today = dt.date.today()
        midnight = dt.datetime.combine(today, dt.datetime.min.time())
        last_minute = dt.datetime.combine(today, dt.datetime.max.time())
        table = DataTable(
            dict(request.query_params), 
            Visit, 
            db.query(Visit).\
                join(Visit.appointment).\
                filter(
                    Visit.check_in < last_minute,
                    Visit.check_in > midnight,
                    Station.name == token.get('station')
                ), [
            'image',
            ('full_name', 'appointment.full_name'),
            'check_in',
            'check_out',
            'id'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/count_all')
def allCount(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')
        count = db.query(Visit).\
                join(Pass, Visit.pass_id == Pass.id).\
                join(Appointment, Pass.appointment_id == Appointment.id).\
                join(Station, Appointment.station_id == Station.id).\
                filter(Station.name == token.get('station')).\
                count()
        
        return {
            'error': False,
            'data': count
        }
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/monthly')
def countMonthly(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        
        temp = db.execute(visit_count_per_month, {'station_name':token.get('station')}).all()
        data = {
            'labels': [],
            'datasets':[
                {
                    'label': '',
                    'data': [0,0,0,0,0,0,0],
                    'fill': False,
                    'borderColor': '#16A34A',
                },
            ]
        }

        data.get('datasets')[0]['label'] = temp[0].station

        
        for i, e in enumerate(temp):
            data.get('labels').append(str(e.month) + '  ' + str(e.year))
            data.get('datasets')[0].get('data')[i] = e.count
            print(i,e)
        return {'data': data}

    except Exception as e:
        # print(e)
        import traceback
        print(traceback.print_exc())
#*------------------------------------------------------------------------------
@router.get('/per_station')
def countPerStation(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        temp = db.execute(visits_per_department).all()
        
        data = {
            'labels': [],
            'datasets': [{
                'label': 'Count',
                'data': [],
                'fill': False,
                'backgroundColor': [
                    '#16A34A',
                    '#6c757d',
                    '#10B981',
                    '#06B6D4',
                    '#EA580C',
                    '#EF4444',
                    '#4a16a3',
                    '#a34a16'
                ]
            }]
        }

        for e in temp:
            data.get('labels').append(e.station)
            data.get('datasets')[0].get('data').append(e.count)

        return {'data': data}
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/by_type')
def countByType(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):  
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    temp = db.query(Visit).\
        join(Pass, Visit.pass_id == Pass.id).\
        join(Appointment, Pass.appointment_id == Appointment.id).\
        with_entities(
            Appointment.type.label('type'),
            func.count(Visit.id).label('count')
        ).\
        filter(Appointment.is_allowed == True).\
        group_by('type').\
        all()

    total = temp[0].count + temp[1].count

    data = {
        'labels': [temp[0].type, temp[1].type],
        'datasets': [{
            'label': 'Count',
            'data': [temp[0].count, temp[1].count],
            'backgroundColor': [
                '#06B6D4',
                '#16A34A'
            ]
        }]
    }

    return {'data': data, 'total': total}
#*------------------------------------------------------------------------------
@router.get('/per_week')
def countPerWeek(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        temp = db.query(Visit).\
            join(Pass, Visit.pass_id == Pass.id).\
            join(Appointment, Pass.appointment_id == Appointment.id).\
            join(Station, Appointment.station_id == Station.id).\
            with_entities(
                func.str_to_date(func.concat(func.yearweek(Visit.check_in), ' Sunday'), '%X%V %W').label('start'),
                func.str_to_date(func.concat(func.yearweek(Visit.check_in), ' Saturday'), '%X%V %W').label('end'),
                func.yearweek(Visit.check_in).label('week'),
                func.count(Visit.id).label('count'),
            ).\
            filter(Station.name == token.get('station')).\
            group_by('week').\
            order_by(desc('week')).\
            limit(16).\
            all()

        data = {
            'labels': [],
            'datasets':[{
                'label': 'Count',
                'data': [],
                'fill': False,
                'borderColor': 'rgb(22,163,74)'
            }]
        }

        for e in temp[::-1]:
            data.get('labels').append(f'{e.start} - {e.end}')
            data.get('datasets')[0].get('data').append(e.count)

        return {'data': data}
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/per_day')
def countPerDay(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    try:
        
        temp = db.execute(visit_count_per_day, {'station_name': token.get('station')}).all()

        data = {
            'labels': [],
            'datasets':[{
                'label': 'Count',
                'data': [],
                'fill': False,
                'borderColor': 'rgb(22,163,74)'
            }]
        }

        for e in temp:
            data.get('labels').append(e.date)
            data.get('datasets')[0].get('data').append(e.count)


        return {'data': data}
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/count_walkin')
def countWalkin(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    try:
        count = db.query(Visit).\
            join(Pass, Visit.pass_id == Pass.id).\
            join(Appointment, Pass.appointment_id == Appointment.id).\
            join(Station, Appointment.station_id == Station.id).\
            filter(
                Appointment.type == 'Walk-In',
                Station.name == token.get('station')
            ).\
            count()

        return count
        
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------

#* POST
#* @Approve Check-in
@router.post('/checkin')
def checkin(
    form: CheckinForm,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
): 
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        data = db.query(Visit).filter(Visit.pass_id == form.pass_id).first()
        if data is not None:
            return {'error': True, 'message': 'This pass has already been checked in.'}
        curpath = os.path.abspath(os.curdir)
        filepath = f'{curpath}/storage/proof/{form.pass_id}.jpg'
        savepath = f'/storage/proof/{form.pass_id}.jpg'
        with open(filepath, 'wb') as file:
            file.write(b64.b64decode(form.image))

        visit = Visit(
            pass_id = form.pass_id,
            image = savepath,
            remarks = form.remarks
        )
        db.add(visit)
        db.commit()

        return {'error': False, 'message': 'Image Uploaded', 'url': '/receptionist/dashboard'}
    except Exception as e:
        print(e)

#* @Approve Check-out
@router.post('/checkout')
def checkout(
    form: CheckoutForm,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        data = db.query(Visit).\
            filter(
                Visit.pass_id == form.pass_id,
                Visit.check_out == None
            ).\
            first()
        if data == None:
            return {'error': True, 'message': 'Invalid Pass'}
        
        data.check_out = dt.datetime.today()
        db.commit()

        return {'error': False, 'message': 'Check-out Successful!'}
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------

#* PUT
@router.put('/details/{id}')
def updateVisitRemarks(
    id: str,
    form: VisitUpdate,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')

        data = db.query(Visit).filter(Visit.id == id).first()

        data.remarks = form.remarks
        db.commit()

        return {'error': False, 'message': 'Visit remarks updated!'}
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
#?------------------------------------------------------------------------------
