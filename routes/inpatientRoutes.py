from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from models import Inpatient
from database import get_db
from dependencies import get_token

router = APIRouter(
    prefix='/inpatients',
    tags=['inpatients'],
)

@router.get('/search')
def search(
    name:str, 
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    builder = ''
    for e in name.split(' '):
        builder = builder + f'+{e} '

    data = db.query(Inpatient).filter(Inpatient.name.match(builder), Inpatient.is_accepting_visits == True).all()

    return len(data)