from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from database import get_db
from models import User, Blacklist
from schemas import AuthForm, RegisterForm
from jose import jwt
from passlib.context import CryptContext

secret = 'a very shady secret'
pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

def password_verify(plain, hashed):
    return pwd_context.verify(plain, hashed)

def password_hash(password):
    return pwd_context.hash(password)

router = APIRouter(
    prefix='/auth',
    tags=['auth']
)

@router.post('/visitor/register')
def register(
    request: RegisterForm,
    response: Response,
    db: Session = Depends(get_db)
):
    try:
        request.password = password_hash(request.password)

        blacklist = db.query(Blacklist).\
            filter(
                Blacklist.is_active == True,
                Blacklist.email == request.email,
            ).\
            first()
            

        user = User(
            email = request.email,
            password = request.password,
            user_type = request.user_type,
            is_blacklist = True if blacklist else False
        )
        db.add(user)
        db.commit()

        if blacklist:
            blacklist.user_id = user.id
            db.commit()

        token = jwt.encode({'user': user.id, 'user_type': 'Visitor'}, secret)
        response.set_cookie('token', token, httponly=True)
        
        return {'error': False, 'message': 'Registered Successfully!'}
    except IntegrityError as e:
        return {'error': True, 'message': 'This email is already taken.'}
    except Exception as e:
        print(e)

@router.post('/receptionist/verify')
def verify(
    form: AuthForm,
    response: Response,
    db: Session = Depends(get_db)
):
    try:
        user = db.query(User).\
            filter(
                User.email == form.email,
                User.user_type == 'Receptionist'
            ).\
            first()
        if user:
            match = password_verify(form.password, user.password)
            if match:
                data = {
                    'id': user.id,
                    'user_type': user.user_type,
                    'station': user.station.name or None,
                    'name': 'Default User' if user.profile is None else user.profile.full_name
                }
                if form.remember:
                    token = jwt.encode({**data, 'expires_delta': 31536000}, secret)
                    response.set_cookie('token', token, httponly=True, max_age=31536000)
                else:
                    token = jwt.encode(data, secret)
                    response.set_cookie('token', token, httponly=True)
                return {'error': False, 'message': 'Log In Success!'}
        
        return {'error': True, 'message': 'User not found.'}
    except Exception as e:
        print(e)

@router.post('/visitor/verify')
def verify(
    form: AuthForm,
    response: Response,
    db: Session = Depends(get_db)
):
    try:
        user = db.query(User).\
            filter(
                User.email == form.email,
                User.user_type == 'Visitor'
            ).\
            first()
        if user:
            match = password_verify(form.password, user.password)
            if match:
                data = {
                    'user': user.id,
                    'user_type': user.user_type,
                    'name': user.profile.full_name if user.profile is not None else 'No Name, Update Profile',
                }
                if form.remember:
                    token = jwt.encode({**data, 'expires_delta': 31536000}, secret)
                    response.set_cookie('token', token, httponly=True, max_age=31536000)
                else:
                    token = jwt.encode(data, secret)
                    response.set_cookie('token', token, httponly=True)
                return {'error': False, 'message': 'Log In Success!'}
        
        return {'error': True, 'message': 'User not found.'}
    except Exception as e:
        print(e)


@router.post('/logout')
def logout(response: Response):
    response.delete_cookie('token')
    return {'error': False, 'message': 'Logout Success!'}