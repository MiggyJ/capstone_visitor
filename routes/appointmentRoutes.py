import datetime as dt
from fastapi import Request, APIRouter, Depends, HTTPException
from sqlalchemy import or_
from sqlalchemy.sql import func
from sqlalchemy.orm import Session, joinedload
from datatables import DataTable
from database import get_db
from models import Appointment, Pass, Station, Health_Form, Blacklist, User
from schemas import OnlineAppointment, RejectAppointment, WalkinAppointment
from dependencies import get_token
from functions import generate_pass

router = APIRouter(
    prefix='/appointments',
    tags=['appointments'],
    dependencies=[Depends(get_token)]
)

#? APPOINTMENTS

#*GET
#* @Datatables Pending - Receptionist
@router.get('/receptionist/datatable_pending')
def appointmentRequestDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:

        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Appointment.full_name.like(f'%{user_input}%'),
                    Station.name.like(f'%{user_input}%'),
                    func.date_format(Appointment.schedule, '%M %d, %Y').like(f'%{user_input}%')
                )
            )
        table = DataTable(dict(request.query_params), Appointment, db.query(Appointment).filter(
            Appointment.status == 'Pending',
            Station.name == token.get('station')
        ), [
            'full_name',
            ('destination', 'station.name'),
            'schedule',
            'id'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e)
#* @Datatables Approved - Receptionist
@router.get('/receptionist/datatable_approved')
def appointmentApprovedTodayDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Appointment.full_name.like(f'%{user_input}%'),
                    Station.name.like(f'%{user_input}%'),
                    func.date_format(Appointment.schedule, '%M %d, %Y').like(f'%{user_input}%'),
                    Appointment.type.like(f'%{user_input}%')
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Appointment, 
            db.query(Appointment).\
                filter(
                    Appointment.status == 'Approved',
                    Station.name == token.get('station')
                ), [
            'full_name',
            ('destination', 'station.name'),
            'schedule',
            'file',
            'type',
            'id'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e)
#* @Datatables Rejected - Receptopnist
@router.get('/receptionist/datatable_rejected')
def appointmentRejectedDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Appointment.full_name.like(f'%{user_input}%'),
                    Station.name.like(f'%{user_input}%'),
                    func.date_format(Appointment.schedule, '%M %d, %Y').like(f'%{user_input}%'),
                    Appointment.type.like(f'%{user_input}%')
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Appointment, 
            db.query(Appointment).\
                filter(
                    Appointment.status == 'Rejected',
                    Station.name == token.get('station')
                ), [
            'full_name',
            ('destination', 'station.name'),
            'schedule',
            'file',
            'type',
            'id'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e)

#* Visitors Datatable
@router.get('/visitor/datatable')
def appointmentVisitorDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    func.date_format(Appointment.schedule, '%M %d, %Y').like(f'%{user_input}%'),
                    Appointment.status.like(f'%{user_input}%'),
                    Station.name.like(f'%{user_input}%'),
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Appointment, 
            db.query(Appointment).\
                filter(
                    Appointment.user_id == token.get('user'),
                    Appointment.schedule >= dt.date.today(),
                ), [
            ('destination', 'station.name'),
            'status',
            'schedule',
            'id',
            'file',
            'is_allowed'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e) 
@router.get('/visitor/history/datatable')
def appointmentVisitorDatatable(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    func.date_format(Appointment.schedule, '%M %d, %Y').like(f'%{user_input}%'),
                    Appointment.status.like(f'%{user_input}%'),
                    Station.name.like(f'%{user_input}%'),
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Appointment, 
            db.query(Appointment).\
                filter(
                    Appointment.user_id == token.get('user'),
                    Appointment.schedule <= dt.date.today(),
                ), [
            ('destination', 'station.name'),
            'status',
            'schedule',
            'id',
            'file',
            'is_allowed'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e) 

#* GET One Appointment
@router.get('/{id}')
def getOneAppointment(
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):

    if not token:
        raise HTTPException(403, detail='You are not allowed to access this page.')

    data = db.query(Appointment).\
            filter(Appointment.id == id).\
            options(
                joinedload(Appointment.station),
                joinedload(Appointment.visit),
                joinedload(Appointment.health_form)
            ).\
            first()


    return {
        'error': False,
        'data': data
    }

@router.get('/receptionist/count_all')
def allCount(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')
        count = db.query(Appointment).\
            join(Station, Station.id == Appointment.station_id).\
            filter(
                Appointment.status == 'Pending',
                Station.name == token.get('station')
            ).\
            count()
        
        return {
            'error': False,
            'data': count,
        }
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/receptionist/count_approve')
def approveCount(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')
        count = db.query(Appointment).\
            join(Station).\
            filter(
                Appointment.status == 'Approved',
                Station.name == token.get('station')
            ).\
            count()
        
        return count
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
@router.get('/receptionist/count_reject')
def rejectCount(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')
        count = db.query(Appointment).\
            join(Station).\
            filter(
                Appointment.status == 'Rejected',
                Station.name == token.get('station')
            ).\
            count()
        
        return count
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
#*POST
#* @Walk-in Appointment
@router.post('/walkin')
def walkinAppointment(
    req: WalkinAppointment,
    db: Session = Depends(get_db)
):
    try:

        blacklist = db.query(Blacklist).\
            filter(
                Blacklist.is_active == True,
                Blacklist.email == req.email,
                Blacklist.full_name == req.full_name,
                Blacklist.birth_date == req.birth_date
            ).\
            first()

        if blacklist:
            return {'error': True, 'message': 'The information provided matches one of our blacklisted person. Please verify if the person in the picture is the visitor.', 'data': blacklist}

        form = Health_Form(
            full_name = req.full_name,
            email = req.email,
            symptoms = req.symptoms,
            condition = req.condition,
            date_submitted = req.date_submitted
        )
        db.add(form)
        db.commit()

        appointment = Appointment(
            station_id = req.station_id,
            full_name = req.full_name,
            email = req.email,
            contact_number = req.contact_number,
            birth_date = req.birth_date,
            address = req.address,
            patient_name = req.patient_name,
            purpose = req.purpose,
            schedule = req.schedule,
            status = req.status,
            date_submitted = req.date_submitted,
            type = req.type,
            file = None
        )
        db.add(appointment)
        db.commit()

        new_pass = Pass(
            appointment_id = appointment.id,
            health_form_id = form.id,
            full_name = req.full_name,
            email = req.email,
        )
        db.add(new_pass)
        db.commit()

        file = generate_pass(new_pass.id, appointment.full_name, appointment.email, appointment.schedule, appointment.station.name)

        appointment.file = file
        db.commit()

        return {'error': False, 'message': 'Appointment added!'}
    except Exception as e:
        print(e)

@router.post('/online')
def onlineAppointment(
    req: OnlineAppointment,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        id = token.get('user')
        user = db.query(User).\
            filter(User.id == id).\
            first()

        data = db.query(Appointment).\
            filter(
                Appointment.user_id == id,
                Appointment.schedule == req.schedule
            ).\
            first()
        
        if data is not None:
            return {
                'error': True, 
                'message': 'You have already requested an appointment on this day.'
            }
        else:

            appointment_count = db.query(Appointment).\
                filter(
                    Appointment.schedule == req.schedule,
                    Appointment.status == 'Approved'
                ).\
                count()

            if appointment_count == 50:
                return {'error': True, 'message': 'Appointments on this day has reached the maximum allowed number. Please try another schedule.'}

            appointment = Appointment(
                user_id = id,
                full_name = user.profile.full_name,
                email = user.email,
                contact_number = user.profile.contact_number,
                birth_date = user.profile.birth_date,
                address = user.profile.full_address,
                station_id = req.station_id,
                patient_name = req.patient_name,
                schedule = req.schedule,
                purpose = req.purpose,
                date_submitted = req.date_submitted,
                type = req.type,
                status = 'Pending'
            )

            db.add(appointment)
            db.commit()

            return {
                'error': False,
                'message': 'Appointment requested successfully!'
            }
    except Exception as e:
        print(e)
#*--------------------------------------------------------------------------------

#* PUT
#* @Approve Appointment
@router.put('/{id}')
def approveAppointment(
    id: str,
    token: dict = Depends(get_token), 
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')

        data = db.query(Appointment).\
            filter(Appointment.id == id).\
            first()
        
        data.status = 'Approved'
        db.commit()
        return {'error': False, 'message': 'Appointment approved!'}
    except Exception as e:
        print(e)

#* DELETE
#* @Reject Appointment
@router.delete('/{id}')
def rejectAppointment(
    id: str,
    req: RejectAppointment,
    token: dict = Depends(get_token), 
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')
        
        db.query(Appointment).\
            filter(Appointment.id == id).\
            update({Appointment.status: 'Rejected', Appointment.remarks: req.remarks})
        db.commit()
        return {'error': False, 'message': 'Appointment rejected'}
    except Exception as e:
        print(e)
@router.delete('/cancel/{id}')
def cancelAppointment(
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        appointment = db.query(Appointment).\
            filter(Appointment.id == id).\
            first()

        if appointment == None:
            return {'error': True, 'message': 'Appointment not found'}
        
        appointment.status = 'Cancelled'
        db.commit()

        return {
            'error': False,
            'message': 'Appointment cancelled successfully'
        }
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------
#?------------------------------------------------------------------------------