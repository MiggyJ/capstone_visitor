from fastapi import Request, APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from database import get_db
from models import Blacklist
from schemas import UpdateBlacklistRemarks
from dependencies import get_token

router = APIRouter(
    prefix='/blacklist',
    tags=['blacklist']
)

@router.get('/remarks/{id}')
def getRemarks(
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        data = db.query(Blacklist).\
            filter(Blacklist.id == id).\
            first()

        if data:
            count = db.query(Blacklist).filter(Blacklist.email == data.email).count()
            return {'error': False, 'data': data, 'count': count}
        else:
            return {'error': True, 'message': 'Blacklist entry not found.'}

    except Exception as e:
        print(e)

@router.put('/seen/{id}')
def seenBlacklist(
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        data = db.query(Blacklist).\
            filter(Blacklist.id == id).\
            first()

        data.is_seen = True
        db.commit()

        return {'error': False}
    except Exception as e:
        print(e)

@router.put('/remarks/{id}')
def updateRemarks(
    req: UpdateBlacklistRemarks,
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        data = db.query(Blacklist).\
        filter(Blacklist.id == id).\
        first()

        data.remarks = req.remarks
        db.commit()

        return {'error': False, 'message': 'Remarks updated successfully!'}

    except Exception as e:
        print(e)
