from fastapi import Request, APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import or_
from sqlalchemy.sql import func
from datatables import DataTable
from models import User, User_Profile, Blacklist, Visit
from schemas import ProfileForm, AddBlacklist
from database import get_db
from dependencies import get_token, is_guest

router = APIRouter(
    prefix='/profile',
    tags=['profile'],
    dependencies=[Depends(get_token)]
)

#* GET
@router.get('/datatable/active')
def getActiveBlacklist(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Blacklist.full_name.like(f'%{user_input}%'),
                    Blacklist.email.like(f'%{user_input}%'),
                    func.date_format(Blacklist.created_at, '%M %d, %Y').like(f'%{user_input}%'),
                    func.date_format(Blacklist.birth_date, '%M %d, %Y').like(f'%{user_input}%'),
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Blacklist, 
            db.query(Blacklist).\
                filter(
                    Blacklist.is_active == True,
                ), [
            ('image'),
            ('full_name', 'full_name'),
            'birth_date',
            'created_at',
            'id'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))


        return table.json()
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------

@router.get('/datatable/blacklist')
def getBlacklistUser(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Blacklist.full_name.like(f'%{user_input}%'),
                    Blacklist.email.like(f'%{user_input}%'),
                    func.date_format(Blacklist.created_at, '%M %d, %Y').like(f'%{user_input}%'),
                    func.date_format(Blacklist.birth_date, '%M %d, %Y').like(f'%{user_input}%'),
                )
            )

        table = DataTable(
            dict(request.query_params), 
            Blacklist, 
            db.query(Blacklist), [
            ('full_name', 'full_name'),
            ('status', 'is_active'),
            ('created_at'),
            'id',
            'user_id',
            'image'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))


        return table.json()
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------

@router.get('/count_all')
def allCount(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')
        count = db.query(User).filter(User.user_type == 'Visitor').count()
        
        return {
            'error': False,
            'data': count
        }
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------

@router.get('/count_blacklist')
def blacklistCount(
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        count = db.query(User).\
            filter(User.is_blacklist == True).\
            count()

        return {
            'error': False,
            'data': count
        }
    except Exception as e:
        print(e)
#*------------------------------------------------------------------------------

#* POST
@router.post('/save')
def saveProfile(
    request: ProfileForm,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        id = token.get('user')
        data = db.query(User_Profile).filter(User_Profile.user_id == id).first()

        if data:
            data.first_name = request.first_name
            data.middle_name = request.middle_name
            data.last_name = request.last_name
            data.suffix_name = request.suffix_name
            data.full_name = request.full_name
            data.house_street = request.house_street
            data.barangay = request.barangay
            data.municipality = request.municipality
            data.province = request.province
            data.region = request.region
            data.full_address = request.full_address
            data.contact_number = request.contact_number
            data.gender = request.gender
            data.birth_date = request.birth_date

            db.commit()
        else:
            new_profile = User_Profile(
                user_id = id,
                first_name = request.first_name,
                middle_name = request.middle_name,
                last_name = request.last_name,
                suffix_name = request.suffix_name,
                full_name = request.full_name,
                house_street = request.house_street,
                barangay = request.barangay,
                municipality = request.municipality,
                province = request.province,
                region = request.region,
                full_address = request.full_address,
                contact_number = request.contact_number,
                gender = request.gender,
                birth_date = request.birth_date,
            )

            db.add(new_profile)
            db.commit()
        
        return {'error': False, 'message': 'Profile updated successfully'}

    except Exception as e:
        print(e)

#* PUT
@router.put('/blacklist/remove/{id}')
def removeBlacklist(
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    try:
        blacklist = db.query(Blacklist).\
            filter(Blacklist.id == id).\
            first()

        if blacklist is None:
            return {'error': True, 'message': 'Blacklist Entry not found.'}

        if blacklist.is_active == False :
            return {'error': True, 'message': 'Blacklist is inactive.'}

        user = db.query(User).\
            filter(User.id == Blacklist.user_id).\
            first()

        blacklist.is_active = False
        user.is_blacklist = False

        db.commit()

        return {'error': False, 'message': 'Blacklist Entry reverted.'}

    except Exception as e:
        print(e)

#*DELETE
@router.delete('/blacklist/')
def addToBlacklist(
    req: AddBlacklist,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        visit = db.query(Visit).\
            filter(Visit.id == req.visit).\
            first()
        
        print(req.visit)

        if visit.user != None:

            if visit.user.is_blacklist == True:
                return {'error': True, 'message': 'User is in blacklist already.'}
            
            else:
                
                blacklist = Blacklist(
                    user_id = visit.user.id,
                    full_name = visit.user.profile.full_name,
                    email = visit.user.email,
                    birth_date = visit.user.profile.birth_date,
                    remarks = req.remarks,
                    image = req.image
                )

                visit.user.is_blacklist = True

                
                db.add(blacklist)
                db.commit()

                for e in visit.user.appointment:
                    if e.status == 'Pending':
                        e.status = 'Rejected'
                        e.remarks = 'You have been blacklisted.'
                        e.is_allowed = False

                db.commit()
            
        else:

            existing = db.query(Blacklist).\
                filter(
                    Blacklist.full_name == visit.appointment.full_name,
                    Blacklist.email == visit.appointment.email,
                    Blacklist.birth_date == visit.appointment.birth_date,
                    Blacklist.is_active == True
                ).first()

            if existing:
                return {'error': True, 'message': 'This visitor is already blacklisted'}

            blacklist = Blacklist(
                full_name = visit.appointment.full_name,
                email = visit.appointment.email,
                birth_date = visit.appointment.birth_date,
                remarks = req.remarks,
                image = req.image
            )

            db.add(blacklist)
            db.commit()
        
        return {'error': False, 'message': 'User added to Blacklist.'}

    except Exception as e:
        print(e)

