from fastapi import Request, APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session, joinedload
import datetime as dt
from database import get_db
from models import Pass, Visit, Appointment
from schemas import ValidatePass
from dependencies import get_token

router = APIRouter(
    prefix='/passes',
    tags=['passes'],
    dependencies=[Depends(get_token)]
)


#? PASSES

#* POST
#* @Scan Pass for Check-In
@router.post('/')
def validatePass(
    form: ValidatePass,
    db: Session = Depends(get_db)
):
    data = db.query(Pass).\
        join(Appointment).\
        filter(
            Pass.id == form.pass_id,
            Appointment.schedule == dt.date.today()
        ).\
        first()
    if data is not None:
        data = db.query(Visit).\
            filter(
                Visit.pass_id == form.pass_id,
                Visit.check_out == None,
            ).\
            first()
        if data is not None:
            return {'error': True, 'message': 'This code is already in.'}
        return {'error': False, 'url': f'/receptionist/checkin/{form.pass_id}'}
    else:
        return {'error': True, 'message': 'Code is invalid.'}
#*------------------------------------------------------------------------------
#?------------------------------------------------------------------------------
