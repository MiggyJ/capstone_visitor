from fastapi import Request, APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette.responses import RedirectResponse
from models import Appointment, Blacklist, Pass, Health_Form, Visit, User
from database import get_db
from templates import template
from dependencies import get_token, is_guest

router = APIRouter(
    prefix='/receptionist',
    tags=['receptionist'],
)

@router.get('/')
def login(
    request: Request, 
    guest = Depends(is_guest)
):
    if guest != True:
        if guest == 'Receptionist':
            return RedirectResponse('/receptionist/dashboard')
        else:
            raise HTTPException(403, 'This page is not for visitors')
    return template.TemplateResponse('layouts/auth/receptionist_login.html', {
        'request': request,
    })

@router.get('/dashboard')
def dashboard(
    request: Request,
    token: dict = Depends(get_token)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    

    return template.TemplateResponse('contents/receptionist/dashboard.html',{
        'request': request,
        'page_title': 'Receptionist Dashboard',
        'page': 'dashboard',
        'name': token.get('name'),
        'station': token.get('station')
    })

@router.get('/analytics')
def analytics(
    request: Request,
    token: dict = Depends(get_token)
):
    try:
        if token.get('user_type') != 'Receptionist':
            raise HTTPException(403, detail='You are not allowed to access this page.')

        return template.TemplateResponse('contents/receptionist/analytics.html',{
            'request': request,
            'page_title': 'Analytics',
            'page': 'analytics',
            'name': token.get('name'),
            'station': token.get('station')
        })
    except Exception as e:
        print(e)
#?------------------------------------------------------------------------------

#? APPOINTMENTS
@router.get('/appointments')
def appointmentIndex(
    request: Request, 
    token: dict = Depends(get_token)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    return template.TemplateResponse('contents/receptionist/appointments.html',{
        'request': request,
        'user_type': token.get('user_type'),
        'page_title': 'Appointments',
        'page': 'appointments',
        'name': token.get('name'),
        'station': token.get('station')
    })

@router.get('/appointment/request/{id}')
def viewRequest(
    id: str, 
    request: Request,
    token: dict = Depends(get_token),
):
    try:

        return template.TemplateResponse('contents/receptionist/appointment_single.html',{
            'request': request,
            'user_type': token.get('user_type'),
            'page_title': 'View Appointment Request',
            'page': 'appointments',
            'name': token.get('name'),
            'station': token.get('station'),
            'appointment_id': id
        })
    except Exception as e:
        print(e)

#?-------------------------------------------------------------------------

#? PASSES
@router.get('/checkin/{id}')
def onePass(
    id: str, 
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
        data = db.query(Pass).\
            filter(Pass.id == id).\
            first()

        if data is None:
            return HTTPException(404)
            
        if data.user is not None:
            latest_form = db.query(Health_Form).\
                filter(Health_Form.user_id == data.user.id).\
                order_by(Health_Form.date_submitted.desc()).\
                first()

            return template.TemplateResponse('contents/receptionist/check_in.html',{
                'request': request,
                'user_type': token.get('user_type'),
                'page_title': 'Receptionist Dashboard',
                'page': 'dashboard',
                'name': token.get('name'),
                'station': token.get('station'),
                'data': data,
                'latest_form': latest_form
            })
        else:
            return template.TemplateResponse('contents/receptionist/check_in.html',{
                'request': request,
                'user_type': token.get('user_type'),
                'page_title': 'Receptionist Dashboard',
                'page': 'dashboard',
                'name': token.get('name'),
                'station': token.get('station'),
                'data': data,
            })
    except Exception as e:
        print(e)
#?-------------------------------------------------------------------------
#?VISITS
#* GET
#* @Get all Visit
@router.get('/visits')
def getAllVisit(
    request: Request, 
    token: dict = Depends(get_token)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    return template.TemplateResponse('contents/receptionist/visits.html',{
        'request': request,
        'user_type': token.get('user_type'),
        'page_title': 'Visits',
        'page': 'visits',
        'name': token.get('name'),
        'station': token.get('station')
    })
    
#* @Get one Visit
@router.get('/visits/{id}')
def getOneVisit(
    request: Request,
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    try:
        data = db.query(Visit).\
            filter(Visit.id == id).\
            first()

        return template.TemplateResponse('contents/receptionist/visit_single.html',{
            'request': request,
            'user_type': token.get('user_type'),
            'page_title': 'Visit',
            'page': 'visits',
            'name': token.get('name'),
            'station': token.get('station'),
            'data': data
        })
    except Exception as e:
        print(e)

#? Users

@router.get('/blacklist')
def blacklistIndex(
    request: Request,
    token: dict = Depends(get_token),
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    return template.TemplateResponse('contents/receptionist/blacklist.html',{
        'request': request,
        'page_title': 'Blacklist',
        'page': 'blacklist',
        'name': token.get('name'),
        'station': token.get('station')
    })

@router.get('/users/{id}')
def getOneUser(
    request: Request,
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):

    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        data = db.query(User).\
            filter(User.id == id).\
            first()

        blacklist = db.query(Blacklist).\
            filter(
                Blacklist.user_id == data.id,
                Blacklist.is_active == True
            ).\
            first()

        return template.TemplateResponse('contents/receptionist/user_single.html',{
            'request': request,
            'page_title': 'User Information',
            'page': 'users',
            'name': token.get('name'),
            'station': token.get('station'),
            'data': data,
            'blacklist': blacklist
        })
    except Exception as e:
        print(e)

#? COVID ALERTS
@router.get('/mailer')
def viewMailer(
    request: Request,
    token: dict = Depends(get_token),
):
    return template.TemplateResponse('contents/receptionist/covid_alert.html',{
        'request': request,
        'page': 'mailer',
        'page_title': 'COVID-19 Alert Mailer',
        'name': token.get('name'),
        'station': token.get('station')
    })
