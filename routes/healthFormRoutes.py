from fastapi import Request, APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from database import get_db
from models import Health_Form, Pass, Visit, Appointment, User
from schemas import HealthForm
from dependencies import get_token
from functions import generate_pass

router = APIRouter(
    prefix='/health_form',
    tags=['health form']
)

@router.post('/store')
def createHealthForm(
    req: HealthForm,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):

    try:
        user = db.query(User).\
            filter(User.id == token.get('user')).\
            first()

        appointment = db.query(Appointment).\
            filter(
                Appointment.id == req.appointment_id
            ).\
            first()

        health_form = Health_Form(
            user_id = user.id,
            full_name = user.profile.full_name,
            email = user.email,
            symptoms = req.symptoms,
            condition = req.condition,
            date_submitted = req.date_submitted
        )

        db.add(health_form)
        db.commit()

        if health_form.condition != 'Severe':
            new_pass = Pass(
                user_id = appointment.user_id,
                appointment_id = appointment.id,
                health_form_id = health_form.id,
                full_name = appointment.full_name,
                email = appointment.email,
            )
            db.add(new_pass)
            db.commit()
            
            file = generate_pass(
                new_pass.id, appointment.full_name, 
                appointment.email, appointment.schedule,
                appointment.station.name
            )

            appointment.file = file
            appointment.is_allowed = True
            db.commit()

            return {'error': False, 'message': 'You can now download/print your pass. Check the actions dropdown to see the options.'}
        else:
            appointment.is_allowed = False
            appointment.status = 'Reject'
            appointment.remarks = 'Bad Health in declaration.'
            db.commit()
            return {'error': True, 'message': 'Your condition is not good enough to be permitted entry inside the hospital. Please re-schedule when you are feeling better.'}
    except Exception as e:
        print(e)