from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from database import get_db
from models import Station
from dependencies import get_token

router = APIRouter(
    prefix='/stations',
    tags=['stations'],
    dependencies=[Depends(get_token)]
)


@router.get('/all')
def allStations(
    db: Session = Depends(get_db),
):
    data = db.query(Station).all()
    return {'error': False, 'data': data}