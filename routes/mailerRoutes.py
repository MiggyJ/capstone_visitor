from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.sql import func
from models import Visit, Appointment, Pass
from schemas import MailForm
from database import get_db
from dependencies import get_token
from sqlalchemy.orm import Session, contains_eager, joinedload
from functions import send_mail


router = APIRouter(
    prefix='/mailer',
    tags=['mailer', 'covid-19 alert']
)


@router.get('/search')
def searchVisitor(
    q: str,
    db: Session = Depends(get_db),
    token: dict = Depends(get_token),
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    builder = ''
    for e in q.split(' '):
        builder = builder + f'+{e} '

    data = db.query(Visit).\
        join(Pass).\
        join(Appointment).\
        options(contains_eager(Visit.visit_pass).contains_eager(Pass.appointment)).\
        filter(Appointment.full_name.match(builder)).order_by(Visit.check_out.desc()).all()
        
    try:
        for e in data:
            print(e.visit_pass.appointment.full_name)
    except Exception as e:
        print(e)

    if data:
        return {
            'data': data,
            'error': False
        }
    else:
        return {
            'message': 'Visitor not found',
            'error': True
        }

@router.post('/send')
def sendAlertMails(
    mail: MailForm,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Receptionist':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    try:
            
        data = db.query(Visit).\
                join(Pass).\
                join(Appointment).\
                with_entities(
                    Appointment.full_name.label('full_name'),
                    Appointment.email.label('email'),
                ).filter(func.date_format(Visit.check_in, '%Y-%m-%d').like(f'%{mail.date}%')).\
                all()

        for e in data:
            if e.full_name == mail.name:
                continue
            send_mail(e.full_name, mail.date, e.email, mail.message)

        return {
            'error': False
        }
    except Exception as e:
        print(e)




