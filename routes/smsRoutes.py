import datetime as dt
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from database import get_db
from dependencies import get_token
from functions import send_sms
from schemas import SMSForm
from models import Visit, Pass, Appointment

router = APIRouter(
  prefix='/sms',
  tags=['sms', 'evacuation']
)

@router.post('/send')
def evacuation_message(
  req: SMSForm,
  db: Session = Depends(get_db),
  token: dict = Depends(get_token),
):
	if token.get('user_type') != 'Receptionist':
		raise HTTPException(403, detail='You are not allowed to access this page.')

	try:
		today = dt.date.today()
		midnight = dt.datetime.combine(today, dt.datetime.min.time())
		last_minute = dt.datetime.combine(today, dt.datetime.max.time())
		visits = db.query(Visit).\
					filter(
						Visit.check_in < last_minute,
						Visit.check_in > midnight,
						Visit.check_out == None
					).all()


		for el in visits:
			send_sms(el.appointment.full_name, '+63'+el.appointment.contact_number, req.hazard, req.station)

		
		return {
			'error': False,
			'message': 'Messages were sent successfully!'
		}
	except Exception as e:
		print(e)