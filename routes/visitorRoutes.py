from fastapi import Request, APIRouter, Depends, HTTPException
from jose import jwt
from sqlalchemy.orm import Session
from starlette.responses import RedirectResponse
from models import User, Appointment, Pass, Health_Form, User_Profile
from database import get_db
from templates import template
from dependencies import get_token, is_guest
import datetime as dt

router = APIRouter(
    prefix='/visitor',
    tags=['visitor'],
)

@router.get('/')
def visitorLogin(
    request: Request,
    guest = Depends(is_guest),
    db: Session = Depends(get_db)
):
    if guest != True:
        if guest == 'Visitor':
            user = jwt.decode(request.cookies['token'], 'a very shady secret')
            data = db.query(User_Profile).\
                filter(User_Profile.user_id == user.get('user')).\
                first()
                
            if data is None:
                return RedirectResponse('/visitor/profile')
            else:
                return RedirectResponse('/visitor/appointments')
        else:
            return RedirectResponse('/receptionist')
            # raise HTTPException(403, 'This page is not for receptionists')
    
    return template.TemplateResponse('layouts/auth/visitor_login.html', {
        'request': request,
    })

@router.get('/register')
def visitorRegister(
    request: Request,
    guest = Depends(is_guest)
):
    if guest != True:
        if guest == 'Visitor':
            return RedirectResponse('/visitor/profile')
        else:
            raise HTTPException(403, 'This page is not for receptionists')
    
    return template.TemplateResponse('layouts/auth/visitor_register.html', {
        'request': request,
    })

@router.get('/appointments')
def visitorDashboard(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        data = db.query(User_Profile).\
            filter(User_Profile.user_id == token.get('user')).\
            first()


        return template.TemplateResponse('/contents/visitor/dashboard.html',{
            'request': request,
            'name': token.get('name'),
            'page_title': 'Your Appointments',
            'page': 'appointments',
            'profile': data
        })
    except Exception as e:
        import traceback
        print(traceback.print_exc())
        # print(e)

@router.get('/history')
def visitorHistory(
    request: Request,
    token: dict = Depends(get_token),
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    return template.TemplateResponse('contents/visitor/history.html',{
        'request': request,
        'name': token.get('name'),
        'page_title': 'Appointment History',
        'page': 'history'
    })
    

@router.get('/profile')
def visitor_register(
    request: Request,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):

    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')
    
    data = db.query(User_Profile).filter(User_Profile.user_id == token.get('user')).first()

    try:

        return template.TemplateResponse('contents/visitor/visitor_profile.jinja2', {
            'request': request,
            'name': token.get('name'),
            'page_title': 'Profile Setup',
            'page': 'profile',
            'data': data,
        })
    except Exception as e:
        print(e)


@router.get('/appointment/{id}')
def oneAppointment(
    request: Request,
    id: str,
    token: dict = Depends(get_token),
    db: Session = Depends(get_db)
):
    if token.get('user_type') != 'Visitor':
        raise HTTPException(403, detail='You are not allowed to access this page.')

    try:
        appointment = db.query(Appointment).\
            filter(Appointment.id == id).\
            first()

        if appointment is None:
            raise HTTPException(404, 'Appointment not found')
        else:
            if appointment.schedule >= dt.date.today():
                page = 'appointments'
            else:
                page = 'history'

            return template.TemplateResponse('contents/visitor/appointment_single.html',{
                'request': request,
                'name': token.get('name'),
                'page_title': 'View Appointment',
                'page': page,
                'data': appointment
            })
    except Exception as e:
        print(e)
