from fastapi.templating import Jinja2Templates


# Register template folder
template = Jinja2Templates('templates')