"""create_fake_inpatient

Revision ID: 0a948c3c1e97
Revises: f26bddf342d5
Create Date: 2022-02-26 12:21:29.950259

"""
from alembic import op
import sqlalchemy as sa
import csv


# revision identifiers, used by Alembic.
revision = '0a948c3c1e97'
down_revision = 'f26bddf342d5'
branch_labels = None
depends_on = None


def upgrade():
    inpatient_table = op.create_table(
        'inpatients',
        sa.Column('id', sa.String(36), primary_key=True, nullable=False, default=sa.text('UUID()')),
        sa.Column('first_name', sa.String(255), nullable=False),
        sa.Column('middle_name', sa.String(255), nullable=True),
        sa.Column('last_name', sa.String(255), nullable=False),
        sa.Column('full_name', sa.String(255), nullable=False),
        sa.Column('is_accepting_visits', sa.Boolean, default=sa.text('1')),
    )

    op.create_index('ft_full_name', 'inpatients', ['full_name'], mysql_prefix='FULLTEXT')

    inpatients = []

    with open('data/fake_inpatients.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")

        for row in csv_reader:
            inpatients.append({
                'id': row['id'],
                'first_name': row['first_name'],
                'last_name': row['last_name'],
                'full_name': row['full_name'],
                'is_accepting_visits': 1
            })


    op.bulk_insert(inpatient_table, [*inpatients])

def downgrade():
    op.drop_table('inpatients')
