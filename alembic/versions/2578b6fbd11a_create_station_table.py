"""create_station_table

Revision ID: 2578b6fbd11a
Revises: cfb3de606a46
Create Date: 2021-08-19 20:21:22.423723

"""
from alembic import op
import sqlalchemy as sa
import csv


# revision identifiers, used by Alembic.
revision = '2578b6fbd11a'
down_revision = 'cfb3de606a46'
branch_labels = None
depends_on = None


def upgrade():
    stations_table = op.create_table(
        'stations',
        sa.Column('id', sa.String(36), primary_key=True, default=sa.text('UUID()')),
        sa.Column('name', sa.String(255), nullable=False),
        sa.Column('location', sa.String(255), nullable=False),
    )

    stations = []
    with open('data/stations.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for row in csv_reader:
            stations.append({
                'id': row['id'],
                'name': row['name'],
                'location': row['location']
            })
    
    op.bulk_insert(stations_table, stations)

    op.add_column(
        'users',
        sa.Column('station_id', sa.String(36), sa.ForeignKey('stations.id'))
    )

    op.execute(f"UPDATE users SET station_id = '{stations[0].get('id')}' WHERE id = 'af712048-c360-4f48-98a2-dabf9db33d85'")
    op.execute(f"UPDATE users SET station_id = '{stations[1].get('id')}' WHERE id = 'af8d9345-3f3f-499b-9596-da82aa7297b6'")
    op.execute(f"UPDATE users SET station_id = '{stations[2].get('id')}' WHERE id = '997b4b97-b4b9-4971-b600-18313528bc1f'")
    op.execute(f"UPDATE users SET station_id = '{stations[3].get('id')}' WHERE id = '998cbc7c-d13a-4768-80ae-b778a7d85c77'")
    op.execute(f"UPDATE users SET station_id = '{stations[4].get('id')}' WHERE id = '5d02fe5e-2d47-45e1-bb46-edd3eb16aa3b'")
    op.execute(f"UPDATE users SET station_id = '{stations[5].get('id')}' WHERE id = '462cabf0-c577-4e97-ab4e-4cb38b7f00da'")

    op.create_check_constraint(
        'CK_RECEPTION_STATION',
        'users',
        "NOT user_type = 'Receptionist' OR station_id IS NOT NULL"
    )

def downgrade():
    op.drop_constraint('CK_RECEPTION_STATION', 'users', type_='check')
    op.drop_constraint('users_ibfk_1', 'users', type_='foreignkey')
    op.drop_column(
        'users',
        'station_id'
    )
    op.drop_table('stations')
