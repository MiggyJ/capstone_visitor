"""create_user_table

Revision ID: 792a0c181b12
Revises: 
Create Date: 2021-08-19 19:49:34.815468

"""
from alembic import op
import sqlalchemy as sa
import csv


# revision identifiers, used by Alembic.
revision = '792a0c181b12'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    users_table = op.create_table(
        'users',
         sa.Column('id',sa.String(36), primary_key=True, nullable=False, default=sa.text('UUID()')),
         sa.Column('email',sa.String(255), unique=True, nullable=False),
         sa.Column('password',sa.String(255), nullable=False),
         sa.Column('user_type',sa.String(255), nullable=False, default='Visitor'),
         sa.Column('created_at',sa.DateTime, default=sa.text('NOW()')),
         sa.Column('updated_at',sa.DateTime, onupdate=sa.text('NOW()')),
         sa.Column('is_blacklist', sa.Boolean, default=sa.text('0'))
    )
    
    users = []
    with open('data/users.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for row in csv_reader:
            users.append({
                'id': row['id'],
                'email': row['email'],
                'password': row['password'],
                'user_type': 'Visitor' if row['station_id'] == '' else 'Receptionist',
            })
    
    op.bulk_insert(
        users_table, 
        [
            *users, 
            {
                'id' : 'af712048-c360-4f48-98a2-dabf9db33d85',
                'email': 'generalward@reception.homis.com',
                'password': '$2a$12$CWCBpD37jotv5hlxz4Jvi.uTqRPBwo/DewSIAoGoSktYSYWh4enwS',
                'user_type': 'Receptionist',
                'is_blacklist': False,
            },
            {
                'id' : 'af8d9345-3f3f-499b-9596-da82aa7297b6',
                'email': 'micu@reception.homis.com',
                'password': '$2a$12$CWCBpD37jotv5hlxz4Jvi.uTqRPBwo/DewSIAoGoSktYSYWh4enwS',
                'user_type': 'Receptionist',
                'is_blacklist': False
            },
            {
                'id' : '997b4b97-b4b9-4971-b600-18313528bc1f',
                'email': 'sicu@reception.homis.com',
                'password': '$2a$12$CWCBpD37jotv5hlxz4Jvi.uTqRPBwo/DewSIAoGoSktYSYWh4enwS',
                'user_type': 'Receptionist',
                'is_blacklist': False
            },
            {
                'id' : 'cc235e86-0b35-11ec-ae1a-2cf05d061de9',
                'email': 'visitor@homis.com',
                'password': '$2a$12$CWCBpD37jotv5hlxz4Jvi.uTqRPBwo/DewSIAoGoSktYSYWh4enwS',
                'user_type': 'Visitor',
                'is_blacklist': False
            }
        ]
    )

def downgrade():
    op.drop_table('users')
