"""create_healthform_table

Revision ID: 7b82a081a39e
Revises: 2578b6fbd11a
Create Date: 2021-08-19 20:41:55.919525

"""
from alembic import op
import sqlalchemy as sa
import datetime as dt
import csv
import random


# revision identifiers, used by Alembic.
revision = '7b82a081a39e'
down_revision = '2578b6fbd11a'
branch_labels = None
depends_on = None


def upgrade():
    healthforms_table = op.create_table(
        'health_forms',
        sa.Column('id', sa.String(36), primary_key=True, default=sa.text('UUID()')),
        sa.Column('user_id', sa.String(36), sa.ForeignKey('users.id'), nullable=True),
        sa.Column('full_name', sa.String(255), nullable=True),
        sa.Column('email', sa.String(255), nullable=True),
        sa.Column('symptoms', sa.String(255), nullable=True),
        sa.Column('condition', sa.String(255), nullable=False),
        sa.Column('date_submitted', sa.Date, nullable=False, default=sa.text('NOW()')),
    )

    health_forms = []
    date = dt.date.today()

    with open('data/health_forms.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for index, row in enumerate(csv_reader):

            to_subtract = index + 1
            sub = 0
            if index > 45:
                pass
            if index % 20 != 0:
                sub = index % 20
            to_subtract = sub if sub!= 0 else to_subtract

            date_submitted = date - dt.timedelta(to_subtract)


            health_forms.append({
                'id': row['id'],
                'user_id': row['user_id'],
                'full_name': row['full_name'],
                'email': row['email'],
                'condition': row['condition'],
                'date_submitted': date_submitted,
            })
    
    op.bulk_insert(healthforms_table, [
        *health_forms,
        {
            'id': '0d647af9-0407-4a7b-8c2b-ba2924eff81d',
            'user_id': '2e084aed-6df4-480a-a629-435d697cf0c0',
            'full_name': 'Blake Arnold Mackenzie',
            'email': 'blakemackenzie@gmail.com',
            'condition': 'Good',
            'date_submitted': dt.date.today() - dt.timedelta(2)
        }
    ])


def downgrade():
    op.drop_table('health_forms')
