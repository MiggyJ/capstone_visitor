"""create_blacklist_table

Revision ID: f26bddf342d5
Revises: 1983b65a13e2
Create Date: 2021-09-06 17:54:21.210806

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql.expression import text


# revision identifiers, used by Alembic.
revision = 'f26bddf342d5'
down_revision = '1983b65a13e2'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'blacklists',
        sa.Column('id', sa.String(36), primary_key=True, default=sa.text('UUID()')),
        sa.Column('user_id', sa.String(36), sa.ForeignKey('users.id'), nullable=True),
        sa.Column('full_name', sa.String(255), nullable=True),
        sa.Column('email', sa.String(255), nullable=True),
        sa.Column('birth_date', sa.Date, nullable=True),
        sa.Column('image', sa.String(255), nullable=True),
        sa.Column('remarks', sa.Text, nullable=False),
        sa.Column('is_active', sa.Boolean, default=text('1')),
        sa.Column('is_seen', sa.Boolean, default=text('0')),
        sa.Column('created_at', sa.DateTime, default=sa.text('NOW()')),
        sa.Column('updated_at', sa.DateTime, onupdate=sa.text('NOW()')),
    )


def downgrade():
    op.drop_table('blacklists')
