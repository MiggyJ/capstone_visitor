"""create visit table

Revision ID: 1983b65a13e2
Revises: f70b1b9cffc8
Create Date: 2021-08-19 22:00:10.495262

"""
from random import randint
from alembic import op
import sqlalchemy as sa
import datetime as dt
import csv


# revision identifiers, used by Alembic.
revision = '1983b65a13e2'
down_revision = 'f70b1b9cffc8'
branch_labels = None
depends_on = None


def upgrade():
    visits_table = op.create_table(
        'visits',
        sa.Column('id', sa.String(36), primary_key=True, default=sa.text('UUID()')),
        sa.Column('pass_id', sa.String(36), sa.ForeignKey('passes.id'), nullable=True),
        sa.Column('image', sa.String(255), nullable=False),
        sa.Column('remarks', sa.Text, nullable=True),
        sa.Column('check_in', sa.DateTime, nullable=False),
        sa.Column('check_out', sa.DateTime, nullable=True),
    )

    visits = []
    date = dt.date.today()
    with open('data/visits.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for index, row in enumerate(csv_reader):

            to_subtract = index + 1
            sub = 0
            if index > 45:
                pass
            elif index % 20 != 0:
                sub = index % 20
            to_subtract = sub if sub != 0 else to_subtract

            date_submitted = date - dt.timedelta(to_subtract)

            visits.append({
                'id': row['id'],
                'pass_id': row['pass_id'],
                'image': row['image'],
                'check_in': date_submitted,
                'check_out': date_submitted
            })
    
    op.bulk_insert(visits_table, [
        *visits,
        {
            'id': '22f56e63-0158-4df6-8363-f85ff0831955',
            'pass_id': '2962b444-77ec-4dfc-bb78-2f5e84d42e5e',
            'image': '/static/img/placeholder_profile.png',
            'check_in': dt.datetime.today(),
            'check_out': None
        }
    ])

def downgrade():
    op.drop_table('visits')
