"""create pass table

Revision ID: f70b1b9cffc8
Revises: ef36513ce7e7
Create Date: 2021-08-19 21:53:03.124307

"""
from alembic import op
import sqlalchemy as sa
import csv


# revision identifiers, used by Alembic.
revision = 'f70b1b9cffc8'
down_revision = 'ef36513ce7e7'
branch_labels = None
depends_on = None


def upgrade():
    passes_table = op.create_table(
        'passes',
        sa.Column('id', sa.String(36), primary_key=True, default=sa.text('UUID()')),
        sa.Column('user_id', sa.String(36), sa.ForeignKey('users.id'), nullable=True),
        sa.Column('appointment_id', sa.String(36), sa.ForeignKey('appointments.id'), nullable=True),
        sa.Column('health_form_id', sa.String(36), sa.ForeignKey('health_forms.id'), nullable=True),
        sa.Column('full_name', sa.String(255), nullable=False),
        sa.Column('email', sa.String(255), nullable=False),
    )

    passes = []
    with open('data/passes.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for row in csv_reader:
            passes.append({
                'id': row['id'],
                'user_id': row['user_id'],
                'appointment_id': row['appointment_id'],
                'health_form_id': row['health_form_id'],
                'full_name': row['full_name'],
                'email': row['email'],
            })
    
    op.bulk_insert(passes_table, [
        *passes,
        {
            'id': '2962b444-77ec-4dfc-bb78-2f5e84d42e5e',
            'user_id': '2e084aed-6df4-480a-a629-435d697cf0c0',
            'appointment_id': '43c2377d-6d55-492c-a169-5d46c6f06f08',
            'health_form_id': '0d647af9-0407-4a7b-8c2b-ba2924eff81d',
            'full_name': 'Blake Arnold Mackenzie',
            'email': 'blakemackenzie@gmail.com',
        }
    ])


def downgrade():
    op.drop_table('passes')