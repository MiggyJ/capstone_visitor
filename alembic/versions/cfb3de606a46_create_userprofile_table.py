"""create_userprofile_table

Revision ID: cfb3de606a46
Revises: 792a0c181b12
Create Date: 2021-08-19 20:05:58.004815

"""
from alembic import op
import sqlalchemy as sa
import csv


# revision identifiers, used by Alembic.
revision = 'cfb3de606a46'
down_revision = '792a0c181b12'
branch_labels = None
depends_on = None


def upgrade():
    profiles_table = op.create_table(
        'user_profiles',
        sa.Column('id', sa.String(36), primary_key=True, nullable=False, default=sa.text('UUID()')),
        sa.Column('user_id', sa.String(36), sa.ForeignKey('users.id'), nullable=True),
        sa.Column('first_name', sa.String(255), nullable=False),
        sa.Column('middle_name', sa.String(255), nullable=True),
        sa.Column('last_name', sa.String(255), nullable=False),
        sa.Column('suffix_name', sa.String(255), nullable=True),
        sa.Column('birth_date', sa.Date, nullable=False),
        sa.Column('gender', sa.String(255), nullable=False),
        sa.Column('house_street', sa.String(255), nullable=False),
        sa.Column('barangay', sa.String(255), nullable=False),
        sa.Column('municipality', sa.String(255), nullable=False),
        sa.Column('province', sa.String(255), nullable=False),
        sa.Column('region', sa.String(255), nullable=False),
        sa.Column('contact_number', sa.String(255), nullable=False),
        sa.Column('full_name', sa.String(255), nullable=False),
        sa.Column('full_address', sa.String(255), nullable=False),
    )

    profiles = []
    with open('data/profiles.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for row in csv_reader:
            profiles.append({
                'id': row['id'],
                'user_id': row['user_id'],
                'first_name': row['first_name'],
                'middle_name': row['middle_name'],
                'last_name': row['last_name'],
                'suffix_name': row['suffix_name'],
                'birth_date': row['birth_date'],
                'gender': row['gender'],
                'house_street': row['house_street'],
                'barangay': row['barangay'],
                'municipality': row['municipality'],
                'province': row['province'],
                'region': row['region'],
                'full_address': row['full_address'],
                'contact_number': row['contact_number'],
                'full_name': row['full_name'],
            })
    
    op.bulk_insert(profiles_table, [
        *profiles,
        {
            'id': 'ecccc7ac-0b35-11ec-ae1a-2cf05d061de9',
            'user_id': 'cc235e86-0b35-11ec-ae1a-2cf05d061de9',
            'first_name': 'John',
            'middle_name': None,
            'last_name': 'Doe',
            'suffix_name': None,    
            'birth_date': '1988-06-04',
            'gender': 'Male',
            'house_street': '2 J.P. Rizal Street',
            'barangay': '035404005',
            'municipality': '035404000',
            'province': '035400000',
            'region': '030000000',
            'contact_number': '9342342344',
            'full_name': 'John Doe',
            'full_address': '2 J.P. Rizal Street, Calibutbut, Bacolor, Pampanga, Central Luzon',
        }
    ])


def downgrade():
    op.drop_table('user_profiles')
