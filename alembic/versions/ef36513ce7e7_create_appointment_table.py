"""create_appointment_table

Revision ID: ef36513ce7e7
Revises: 7b82a081a39e
Create Date: 2021-08-19 21:44:08.573766

"""
from alembic import op
import sqlalchemy as sa
import datetime as dt
import random
import csv


# revision identifiers, used by Alembic.
revision = 'ef36513ce7e7'
down_revision = '7b82a081a39e'
branch_labels = None
depends_on = None

stations = ['e79ec99e-47a2-4b27-8a1f-d39400e712eb',
'349a786f-6402-44ef-aa26-e0e062301352',
'd34582bc-b972-48c4-990f-ad755b1951a9',
'279d0919-9a3f-47b4-81ce-094f56c8e92c',
'5a0aecfc-a94b-4e9f-9d99-fe09492d325f',
'19b4541c-231e-40c5-a291-526ceed370dc']

def upgrade():
    appointments_table = op.create_table(
        'appointments',
        sa.Column('id', sa.String(36), primary_key=True, default=sa.text('UUID()')),
        sa.Column('user_id', sa.String(36), sa.ForeignKey('users.id'), nullable=True),
        sa.Column('station_id', sa.String(36), sa.ForeignKey('stations.id'), nullable=True),
        sa.Column('full_name', sa.String(255), nullable=False),
        sa.Column('email', sa.String(255), nullable=False),
        sa.Column('contact_number', sa.String(255), nullable=False),
        sa.Column('birth_date', sa.Date, nullable=False),
        sa.Column('address', sa.String(255), nullable=False),
        sa.Column('patient_name', sa.String(255), nullable=True),
        sa.Column('purpose', sa.String(255), nullable=False),
        sa.Column('schedule', sa.Date, nullable=False),
        sa.Column('status', sa.String(255), default='Pending'),
        sa.Column('date_submitted', sa.Date, nullable=False),
        sa.Column('type', sa.String(255), nullable=False),
        sa.Column('file', sa.String(255), nullable=True),
        sa.Column('remarks', sa.Text, nullable=True),
        sa.Column('is_allowed', sa.Boolean, nullable=True),
    )   

    op.create_index('ft_name', 'appointments', ['full_name'], mysql_prefix='FULLTEXT')

    
    appointments = []
    patients = []
    with open ('data/fake_inpatients.csv') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for row in csv_reader:
            patients.append(row['full_name'])

    date = dt.date.today()
    with open('data/appointments.csv', encoding='utf-8-sig') as file:
        csv_reader = csv.DictReader(file, delimiter=",")
        for index, row in enumerate(csv_reader):

            to_subtract = index + 1
            sub = 0
            if index > 45:
                pass
            elif index % 20 != 0:
                sub = index % 20
            to_subtract = sub if sub != 0 else to_subtract

            date_submitted = date - dt.timedelta(to_subtract)

            station_id = stations[random.randint(0,5)]

            appointments.append({
                'id': row['id'],
                'user_id': row['user_id'],
                'station_id': station_id,
                'full_name': row['full_name'],
                'email': row['email'],
                'contact_number': row['contact_number'],
                'birth_date': '1992-03-03',
                'address': row['address'],
                'patient_name': patients[random.randint(0,200)],
                'purpose': row['purpose'],
                'schedule': date_submitted,
                'status': 'Approved',
                'date_submitted': date_submitted,
                'type': 'Online' if random.randint(0,2) % 2 == 0 else 'Walk-In',
                'is_allowed': 1
            })
    
    op.bulk_insert(appointments_table, [
        *appointments,
        {
            'id': '74f253ac-a384-46af-b168-36586c5e002b',
            'user_id': 'cc235e86-0b35-11ec-ae1a-2cf05d061de9',
            'station_id': 'd34582bc-b972-48c4-990f-ad755b1951a9',
            'full_name': 'John Doe',
            'email': 'visitor@homis.com',
            'contact_number': '9342342344',
            'birth_date': '1992-03-03',
            'address': '75 Cedar Lane, Commonwealth, Quezon City, NCR',
            'schedule': date,
            'purpose': 'Visit',
            'patient_name': 'Jane Doe',
            'status': 'Pending',
            'date_submitted': date - dt.timedelta(2),
            'type': 'Online',
            'is_allowed': None,
        },
        {
            'id': '4f5b6bbb-40bc-4f18-aa0f-c004fe03c355',
            'user_id': 'cc235e86-0b35-11ec-ae1a-2cf05d061de9',
            'station_id': 'd34582bc-b972-48c4-990f-ad755b1951a9',
            'full_name': 'John Doe',
            'email': 'visitor@homis.com',
            'contact_number': '9342342344',
            'birth_date': '1992-03-03',
            'address': '75 Cedar Lane, Commonwealth, Quezon City, NCR',
            'schedule': date,
            'purpose': 'Visit',
            'patient_name': 'Jonathan Doe',
            'status': 'Pending',
            'date_submitted': date - dt.timedelta(2),
            'type': 'Online',
            'is_allowed': None,
        },
        {
            'id': '43c2377d-6d55-492c-a169-5d46c6f06f08',
            'user_id': '2e084aed-6df4-480a-a629-435d697cf0c0',
            'station_id': 'd34582bc-b972-48c4-990f-ad755b1951a9',
            'full_name': 'Blake Arnold Mackenzie',
            'email': 'blakemackenzie@gmail.com',
            'contact_number': '9702099933',
            'birth_date': '1981-02-05',
            'address': '98 Essex Court, Commonwealth, Quezon City, NCR',
            'schedule': date,
            'purpose': 'Visit',
            'patient_name': 'Jane Doe',
            'status': 'Approved',
            'date_submitted': date - dt.timedelta(2),
            'type': 'Online',
            'is_allowed': 1
        }
    ])

def downgrade():
    op.drop_table('appointments')
