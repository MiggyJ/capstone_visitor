from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://root@localhost/visitor_management"

engine = create_engine(SQLALCHEMY_DATABASE_URL, pool_size=20, max_overflow=0)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

def get_db():
    db = SessionLocal()
    try:
        yield db
    except:
        db.close()


visit_count_per_day = '''
    SELECT DATE_FORMAT(d.date, '%M %d, %Y') AS date,  COUNT(a.id) AS count, s2.name as station FROM (
        SELECT (CURDATE() - INTERVAL c.number DAY) AS date
            FROM (SELECT singles + tens + hundreds number FROM 
                ( SELECT 0 singles
                UNION ALL SELECT   1 UNION ALL SELECT   2 UNION ALL SELECT   3
                UNION ALL SELECT   4 UNION ALL SELECT   5 UNION ALL SELECT   6
                UNION ALL SELECT   7 UNION ALL SELECT   8 UNION ALL SELECT   9
                ) singles JOIN 
                (SELECT 0 tens
                UNION ALL SELECT  10 UNION ALL SELECT  20 UNION ALL SELECT  30
                UNION ALL SELECT  40 UNION ALL SELECT  50 UNION ALL SELECT  60
                UNION ALL SELECT  70 UNION ALL SELECT  80 UNION ALL SELECT  90
                ) tens  JOIN 
                (SELECT 0 hundreds
                UNION ALL SELECT  100 UNION ALL SELECT  200 UNION ALL SELECT  300
                UNION ALL SELECT  400 UNION ALL SELECT  500 UNION ALL SELECT  600
                UNION ALL SELECT  700 UNION ALL SELECT  800 UNION ALL SELECT  900
                ) hundreds
            ORDER BY number DESC) c  
            WHERE c.number BETWEEN 0 and 180
    )AS d 
    	CROSS JOIN (
        	SELECT s.name as name, s.id as id FROM stations s
        ) s2
        LEFT JOIN visits v ON d.date = v.check_in
        LEFT JOIN passes p ON p.id = v.pass_id
        LEFT JOIN appointments a ON s2.id = a.station_id AND 
        	a.id = p.appointment_id
    WHERE s2.name = :station_name AND d.date < CURDATE()
    GROUP BY s2.name, d.date, v.check_in
    ORDER BY d.date ASC
'''

visit_count_per_month = '''
    SELECT 
	CAST(date_format(d.date, '%c') AS INTEGER) as monthNumber, 
    CAST(date_format(d.date, '%Y') AS INTEGER) as year, 
    date_format(d.date, '%M') as month,
    s2.name as station,
    count(a.id) as count 
    FROM (
        SELECT (CURDATE() - INTERVAL c.number DAY) AS date
            FROM (SELECT singles + tens + hundreds number FROM 
                ( SELECT 0 singles
                UNION ALL SELECT   1 UNION ALL SELECT   2 UNION ALL SELECT   3
                UNION ALL SELECT   4 UNION ALL SELECT   5 UNION ALL SELECT   6
                UNION ALL SELECT   7 UNION ALL SELECT   8 UNION ALL SELECT   9
                ) singles JOIN 
                (SELECT 0 tens
                UNION ALL SELECT  10 UNION ALL SELECT  20 UNION ALL SELECT  30
                UNION ALL SELECT  40 UNION ALL SELECT  50 UNION ALL SELECT  60
                UNION ALL SELECT  70 UNION ALL SELECT  80 UNION ALL SELECT  90
                ) tens  JOIN 
                (SELECT 0 hundreds
                UNION ALL SELECT  100 UNION ALL SELECT  200 UNION ALL SELECT  300
                UNION ALL SELECT  400 UNION ALL SELECT  500 UNION ALL SELECT  600
                UNION ALL SELECT  700 UNION ALL SELECT  800 UNION ALL SELECT  900
                ) hundreds
            ORDER BY number DESC) c  
            WHERE c.number BETWEEN 0 and 180
    ) d 
    	CROSS JOIN (
        	SELECT s.name AS name, s.id AS id FROM stations s
        ) s2
        LEFT JOIN visits v ON d.date = v.check_in
        LEFT JOIN passes p ON p.id = v.pass_id
        LEFT JOIN appointments a ON s2.id = a.station_id AND 
        	a.id = p.appointment_id
        WHERE s2.name = :station_name AND
            DATE_FORMAT(d.date, '%c') > DATE_FORMAT(CURDATE(), '%c') - 6
        GROUP BY s2.name, month
        ORDER BY year ASC, monthNumber ASC
'''

visits_per_department = '''
    SELECT COUNT(a.station_id) as count, s.name as station 
        FROM stations s 
        LEFT JOIN appointments a ON s.id = a.station_id 
        GROUP BY s.id
        ORDER BY s.name
'''