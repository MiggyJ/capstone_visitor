import os
import datetime as dt
from dotenv import dotenv_values

env = dotenv_values('.env')

def generate_pass(id, name, email, schedule, station):
    import qrcode
    from qrcode.image.styledpil import StyledPilImage
    from qrcode.image.styles.moduledrawers import CircleModuleDrawer

    from PIL import Image, ImageDraw, ImageFont
    image = Image.new('RGB', (1013, 638), (255, 255, 255))
    draw = ImageDraw.Draw(image)

    font = ImageFont.truetype('arial.ttf', size=54)

    (x, y) = (363, 440)
    message = 'Visitor Pass'
    color = 'rgb(255,255,255)'
    h,w = font.getsize(message)
    draw.rectangle((0,y-10,1013,y+w+20),fill='black')
    draw.text((1013/2,y), message, fill=color, font=font, anchor='ma')

    font = ImageFont.truetype('arial.ttf', size=42)
    (x, y) = (363, 520)
    message = station
    color = 'rgb(255,255,255)'
    h,w = font.getsize(message)
    draw.rectangle((0,y-10,1013,y+w+20+h),fill='green')
    draw.text((1013/2,y+30), message, fill=color, font=font, anchor='ma')

    (x, y) = (50, 50)
    message = str('Name:\n' + str(name))
    color = 'rgb(0, 0, 0)'  # black color
    font = ImageFont.truetype('arial.ttf', size=40)
    draw.text((x, y), message, fill=color, font=font)

    (x, y) = (50, 160)
    message = str('Email:\n' + str(email))
    color = 'rgb(0, 0, 0)'  # black color
    draw.text((x, y), message, fill=color, font=font)

    (x, y) = (50, 270)
    message = str('Date of Appointment:\n' + schedule.strftime('%B %d, %Y'))
    color = 'rgb(0, 0, 0)'
    draw.text((x, y), message, fill=color, font=font)

    # save the edited image

    image.save(f'./storage/passes/{id}.png')

    qr = qrcode.QRCode(
        version=1,
        box_size=8,
        border=2,
        error_correction=qrcode.ERROR_CORRECT_L
    )
    qr.add_data(id)
    img = qr.make_image(
        image_factory=StyledPilImage,
        module_drawer=CircleModuleDrawer()    
    )
    img.save(id + '.bmp')

    til = Image.open(f'./storage/passes/{id}.png')
    im = Image.open(id + '.bmp')
    til.paste(im, (700, 40))
    til.save(f'./storage/passes/{id}.png')

    os.remove(id+'.bmp')
    return (f'/storage/passes/{id}.png')


def send_mail(name, date, recipient, email):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.utils import formatdate

    text = '''
Important Notice:

Greetings, {name}

The World Health Organization (WHO) increases its support to the national government, through the Department of Interior and Local Government (DILG) and the Department of Health (DOH), with the Contact Tracing Handbook for more than 1,700 local government units (LGUs) to enhance their local COVID-19 contact tracing systems and response strategies.

The PUP Hospital, the management, and its employees are one with the government's effort in strengtening the Contact Tracing Management to help lower the number of cases of COVID-19.

In this regard, we would like to inform you that a visitor last <b>{visit_date}</b> tested <b>POSITIVE</b> for COVID-19. The COVID-19 Test conducted to the said visitor was administered last <b id="date_administered">(Enter Day of Week and Date)</b>.

We encourage you to report on your nearest Health Center or in our Emergency Room for a COVID-19 Test.

Expect a call or email from our Health Workers within 24 hours.


If you are experiencing the following:
    Most common symptoms:
    
        fever
        cough
        tiredness
        loss of taste or smell

    Less common symptoms:
    
        sore throat
        headache
        aches and pains
        diarrhoea
        a rash on skin, or discolouration of fingers or toes
        red or irritated eyes

    Serious symptoms:
    
        difficulty breathing or shortness of breath
        loss of speech or mobility, or confusion
        chest pain


Seek immediate medical attention if you have serious symptoms. Always call before visiting your doctor or health facility.

For more question, please contact +63 912 345 6789 or send an email on qwerty@hospital.com.

Thank you and stay safe.
'''

    date = date.strftime('%B %d, %Y')

    #* Load SMTP
    sender = 'HomIES Admin <noreply@homies.com>'
    receiver = recipient

    message = MIMEMultipart('alternative')
    message['From']    = sender
    message['Subject'] = 'COVID-19 Alert'
    message['To']      = f'{name} <{receiver}>'
    message['Date']    = formatdate()
    message.attach(MIMEText(text.format(name=name, visit_date=date), 'text'))
    message.attach(MIMEText('<!DOCTYPE html><html><body>'+email.format(name=name, visit_date=date)+'</body></html>', 'html'))

    with smtplib.SMTP(env.get('SMTP_HOST'), env.get('SMTP_PORT')) as server:
        server.login(env.get('SMTP_USER'), env.get('SMTP_PASSWORD'))
        server.sendmail(sender, receiver, message.as_string())

def send_sms(name, number, hazard, station):
    from twilio.rest import Client

    client = Client(env.get('TWILIO_TEST_SID'), env.get('TWILIO_TEST_TOKEN'))

    message_bodies = {
        'fire': f'EMERGENCY! Fire! A fire has been reported near the {station}. If you are in the {station}, evacuate immediately. If you are not in the area, stay clear of the {station} so that emergency units and firefighters can work unimpeded. Follow instructions from hospital personnel or local authorities. End of message. Sent to {number}',
        'hostile': f'A hostile intruder with unknown intentions may be inside the hospital near {station}. Be aware of your surroundings, secure yourself behind locked doors, find other shelter or leave the hospital immediately. Follow instructions from emergency or other hospital personnel. Please limit phone use so phone lines are available for emergency messaging. End of message. Sent to {number}',
        'bomb': f'EMERGENCY! A bomb has been found near the {station}. If you are in the vicinity of the {station}, prepare immediately for possible evacuation. If you are not in the area, stay away. Listen for instructions from hospital personnel or local authorities and follow them quickly and carefully. Repeat, a bomb has been found near the {station}. End of message. Sent to {number}',
        'explosion': f'EMERGENCY! There has been an explosion near the {station}. If you are in the immediate vicinity, you should evacuate as instructed to by hospital personnel or local authorities. If you are not in the area, you must keep at a safe distance so that emergency units can work unimpeded. Follow instructions from hospital personnel or local authorities. End of message. Sent to {number}'
    }

    message = client.messages.create(
        body=message_bodies.get(hazard.split(' ')[0].lower()),
        from_=env.get('TWILIO_TEST_NUMBER'),
        to=number
    )

    print(f'Message From SYSTEM\n\nTO:{name} <{number}>\nBODY: {message.body}\nStatus:{message.status}')